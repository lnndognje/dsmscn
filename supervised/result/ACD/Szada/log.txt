epoch 0, test loss is 0.3114,  test sen is 0.0000, test spe is 0.0000, test F1 is nan, test acc is 0.9416, test kappa is -0.0000, 
epoch 1, test loss is 0.2322,  test sen is 0.0000, test spe is 0.0000, test F1 is nan, test acc is 0.9417, test kappa is 0.0000, 
epoch 2, test loss is 0.2363,  test sen is 0.0006, test spe is 1.0000, test F1 is 0.0012, test acc is 0.9417, test kappa is 0.0011, 
epoch 3, test loss is 0.2135,  test sen is 0.0148, test spe is 0.5906, test F1 is 0.0288, test acc is 0.9419, test kappa is 0.0261, 
epoch 4, test loss is 0.2223,  test sen is 0.1913, test spe is 0.4155, test F1 is 0.2620, test acc is 0.9371, test kappa is 0.2338, 
epoch 5, test loss is 0.2383,  test sen is 0.4484, test spe is 0.3489, test F1 is 0.3925, test acc is 0.9190, test kappa is 0.3498, 
epoch 6, test loss is 0.2229,  test sen is 0.4811, test spe is 0.3737, test F1 is 0.4206, test acc is 0.9227, test kappa is 0.3799, 
epoch 7, test loss is 0.2523,  test sen is 0.6110, test spe is 0.3374, test F1 is 0.4347, test acc is 0.9073, test kappa is 0.3887, 
epoch 8, test loss is 0.2471,  test sen is 0.6569, test spe is 0.3466, test F1 is 0.4538, test acc is 0.9077, test kappa is 0.4086, 
epoch 9, test loss is 0.2172,  test sen is 0.5167, test spe is 0.4019, test F1 is 0.4521, test acc is 0.9269, test kappa is 0.4136, 
epoch 10, test loss is 0.2208,  test sen is 0.6065, test spe is 0.3799, test F1 is 0.4672, test acc is 0.9193, test kappa is 0.4260, 
epoch 11, test loss is 0.2427,  test sen is 0.7156, test spe is 0.3470, test F1 is 0.4673, test acc is 0.9048, test kappa is 0.4219, 
epoch 13, test loss is 0.2236,  test sen is 0.6318, test spe is 0.3881, test F1 is 0.4808, test acc is 0.9204, test kappa is 0.4404, 
epoch 18, test loss is 0.1719,  test sen is 0.4474, test spe is 0.5007, test F1 is 0.4726, test acc is 0.9417, test kappa is 0.4418, 
epoch 19, test loss is 0.2217,  test sen is 0.7203, test spe is 0.3629, test F1 is 0.4826, test acc is 0.9099, test kappa is 0.4391, 
epoch 21, test loss is 0.2170,  test sen is 0.7458, test spe is 0.3732, test F1 is 0.4975, test acc is 0.9121, test kappa is 0.4551, 
epoch 23, test loss is 0.1712,  test sen is 0.6369, test spe is 0.4169, test F1 is 0.5039, test acc is 0.9268, test kappa is 0.4663, 
epoch 25, test loss is 0.1876,  test sen is 0.7378, test spe is 0.4007, test F1 is 0.5194, test acc is 0.9203, test kappa is 0.4800, 
epoch 26, test loss is 0.1638,  test sen is 0.6353, test spe is 0.4493, test F1 is 0.5263, test acc is 0.9333, test kappa is 0.4916, 
epoch 32, test loss is 0.1658,  test sen is 0.6849, test spe is 0.4304, test F1 is 0.5286, test acc is 0.9287, test kappa is 0.4922, 
epoch 33, test loss is 0.1596,  test sen is 0.6413, test spe is 0.4071, test F1 is 0.4980, test acc is 0.9246, test kappa is 0.4594, 
epoch 34, test loss is 0.1420,  test sen is 0.5329, test spe is 0.4770, test F1 is 0.5034, test acc is 0.9387, test kappa is 0.4708, 
epoch 60, test loss is 0.1487,  test sen is 0.2653, test spe is 0.5310, test F1 is 0.3539, test acc is 0.9435, test kappa is 0.3277, 
epoch 83, test loss is 0.1508,  test sen is 0.4252, test spe is 0.5249, test F1 is 0.4698, test acc is 0.9440, test kappa is 0.4406, 
epoch 129, test loss is 0.1434,  test sen is 0.2163, test spe is 0.5543, test F1 is 0.3112, test acc is 0.9441, test kappa is 0.2878, 
epoch 193, test loss is 0.2455,  test sen is 0.3200, test spe is 0.5422, test F1 is 0.4025, test acc is 0.9446, test kappa is 0.3754, 
best loss is 0.1420, best F1 is 0.5286, best acc is 0.9446, best kappa is 0.4922, 
learning rate 0.0002, Max Epoch is 200
epoch 0, test loss is 0.3355,  test sen is 0.0000, test spe is 0.0000, test F1 is nan, test acc is 0.9417, test kappa is 0.0000, 
epoch 1, test loss is 0.3088,  test sen is 0.0000, test spe is 0.0000, test F1 is nan, test acc is 0.9417, test kappa is 0.0000, 
epoch 2, test loss is 0.2574,  test sen is 0.0000, test spe is 0.0000, test F1 is nan, test acc is 0.9417, test kappa is 0.0000, 
epoch 3, test loss is 0.2282,  test sen is 0.0000, test spe is 0.0000, test F1 is nan, test acc is 0.9417, test kappa is 0.0000, 
epoch 4, test loss is 0.2076,  test sen is 0.0000, test spe is 0.0000, test F1 is nan, test acc is 0.9417, test kappa is 0.0000, 
epoch 5, test loss is 0.1736,  test sen is 0.0000, test spe is 0.0000, test F1 is nan, test acc is 0.9417, test kappa is 0.0000, 
epoch 6, test loss is 0.1687,  test sen is 0.0000, test spe is 0.0000, test F1 is nan, test acc is 0.9417, test kappa is 0.0000, 
epoch 8, test loss is 0.1546,  test sen is 0.0000, test spe is 0.0000, test F1 is nan, test acc is 0.9417, test kappa is 0.0000, 
epoch 10, test loss is 0.1564,  test sen is 0.0006, test spe is 0.8571, test F1 is 0.0012, test acc is 0.9417, test kappa is 0.0011, 
epoch 11, test loss is 0.1740,  test sen is 0.0009, test spe is 0.9474, test F1 is 0.0018, test acc is 0.9417, test kappa is 0.0016, 
epoch 12, test loss is 0.1724,  test sen is 0.0138, test spe is 0.8017, test F1 is 0.0272, test acc is 0.9423, test kappa is 0.0252, 
epoch 13, test loss is 0.1597,  test sen is 0.0177, test spe is 0.8231, test F1 is 0.0347, test acc is 0.9425, test kappa is 0.0323, 
epoch 14, test loss is 0.1555,  test sen is 0.0717, test spe is 0.7005, test F1 is 0.1301, test acc is 0.9440, test kappa is 0.1205, 
epoch 15, test loss is 0.1566,  test sen is 0.0729, test spe is 0.6941, test F1 is 0.1320, test acc is 0.9440, test kappa is 0.1223, 
epoch 16, test loss is 0.1521,  test sen is 0.1139, test spe is 0.6589, test F1 is 0.1942, test acc is 0.9449, test kappa is 0.1801, 
epoch 18, test loss is 0.1517,  test sen is 0.1232, test spe is 0.6532, test F1 is 0.2072, test acc is 0.9450, test kappa is 0.1923, 
epoch 20, test loss is 0.1536,  test sen is 0.1320, test spe is 0.6614, test F1 is 0.2201, test acc is 0.9454, test kappa is 0.2046, 
epoch 21, test loss is 0.1535,  test sen is 0.1523, test spe is 0.6524, test F1 is 0.2469, test acc is 0.9458, test kappa is 0.2299, 
epoch 22, test loss is 0.1511,  test sen is 0.1239, test spe is 0.6676, test F1 is 0.2090, test acc is 0.9453, test kappa is 0.1943, 
epoch 23, test loss is 0.1422,  test sen is 0.1737, test spe is 0.6630, test F1 is 0.2752, test acc is 0.9466, test kappa is 0.2572, 
epoch 28, test loss is 0.1418,  test sen is 0.1418, test spe is 0.7083, test F1 is 0.2363, test acc is 0.9465, test kappa is 0.2211, 
epoch 29, test loss is 0.1410,  test sen is 0.1284, test spe is 0.7285, test F1 is 0.2184, test acc is 0.9464, test kappa is 0.2044, 
epoch 30, test loss is 0.1388,  test sen is 0.1679, test spe is 0.6916, test F1 is 0.2701, test acc is 0.9471, test kappa is 0.2531, 
epoch 33, test loss is 0.1317,  test sen is 0.1927, test spe is 0.6829, test F1 is 0.3006, test acc is 0.9477, test kappa is 0.2821, 
epoch 34, test loss is 0.1316,  test sen is 0.1901, test spe is 0.6764, test F1 is 0.2968, test acc is 0.9474, test kappa is 0.2783, 
epoch 35, test loss is 0.1311,  test sen is 0.1429, test spe is 0.6855, test F1 is 0.2365, test acc is 0.9462, test kappa is 0.2208, 
epoch 36, test loss is 0.1272,  test sen is 0.3207, test spe is 0.6153, test F1 is 0.4217, test acc is 0.9487, test kappa is 0.3976, 
epoch 37, test loss is 0.1269,  test sen is 0.3747, test spe is 0.5896, test F1 is 0.4582, test acc is 0.9483, test kappa is 0.4325, 
epoch 38, test loss is 0.1267,  test sen is 0.3424, test spe is 0.6125, test F1 is 0.4392, test acc is 0.9490, test kappa is 0.4147, 
epoch 39, test loss is 0.1257,  test sen is 0.3057, test spe is 0.6483, test F1 is 0.4155, test acc is 0.9498, test kappa is 0.3928, 
epoch 40, test loss is 0.1234,  test sen is 0.2929, test spe is 0.6556, test F1 is 0.4049, test acc is 0.9498, test kappa is 0.3827, 
epoch 41, test loss is 0.1258,  test sen is 0.4235, test spe is 0.6007, test F1 is 0.4968, test acc is 0.9499, test kappa is 0.4713, 
epoch 42, test loss is 0.1270,  test sen is 0.4882, test spe is 0.5714, test F1 is 0.5265, test acc is 0.9488, test kappa is 0.4996, 
epoch 45, test loss is 0.1272,  test sen is 0.5036, test spe is 0.5524, test F1 is 0.5269, test acc is 0.9472, test kappa is 0.4990, 
epoch 54, test loss is 0.1227,  test sen is 0.4415, test spe is 0.5751, test F1 is 0.4995, test acc is 0.9484, test kappa is 0.4728, 
epoch 56, test loss is 0.1274,  test sen is 0.5422, test spe is 0.5188, test F1 is 0.5302, test acc is 0.9439, test kappa is 0.5005, 
epoch 60, test loss is 0.1327,  test sen is 0.5627, test spe is 0.5054, test F1 is 0.5325, test acc is 0.9424, test kappa is 0.5019, 
epoch 62, test loss is 0.1226,  test sen is 0.3991, test spe is 0.5943, test F1 is 0.4775, test acc is 0.9490, test kappa is 0.4518, 
epoch 68, test loss is 0.1304,  test sen is 0.6058, test spe is 0.4788, test F1 is 0.5349, test acc is 0.9385, test kappa is 0.5024, 
epoch 103, test loss is 0.1221,  test sen is 0.3066, test spe is 0.6443, test F1 is 0.4155, test acc is 0.9497, test kappa is 0.3926, 
best loss is 0.1221, best F1 is 0.5349, best acc is 0.9499, best kappa is 0.5024, 
learning rate 0.0002, Max Epoch is 200
epoch 0, test loss is 0.2953,  test sen is 0.0000, test spe is 0.0000, test F1 is nan, test acc is 0.1935, 
epoch 0, test loss is 0.3595,  test sen is 0.0001, test spe is 0.0900, test F1 is nan, test acc is 0.0057, 
epoch 1, test loss is 0.2873,  test sen is 0.0000, test spe is 0.0000, test F1 is nan, test acc is 0.0141, 
epoch 2, test loss is 0.2506,  test sen is 0.0000, test spe is 0.0000, test F1 is nan, test acc is 0.0220, 
epoch 3, test loss is 0.2239,  test sen is 0.0000, test spe is 0.0000, test F1 is nan, test acc is 0.0066, 
epoch 4, test loss is 0.2054,  test sen is 0.0000, test spe is 0.0000, test F1 is nan, test acc is 0.0028, 
epoch 5, test loss is 0.1903,  test sen is 0.0000, test spe is 0.0000, test F1 is nan, test acc is 0.0048, 
epoch 6, test loss is 0.1880,  test sen is 0.0000, test spe is 0.0000, test F1 is nan, test acc is 0.0169, 
epoch 8, test loss is 0.1986,  test sen is 0.0000, test spe is 0.0000, test F1 is nan, test acc is 0.0446, 
epoch 9, test loss is 0.1977,  test sen is 0.0000, test spe is 0.0000, test F1 is nan, test acc is 0.0548, 
epoch 11, test loss is 0.1763,  test sen is 0.0000, test spe is 0.0000, test F1 is nan, test acc is 0.0156, 
epoch 17, test loss is 0.1729,  test sen is 0.0037, test spe is 0.3780, test F1 is nan, test acc is 0.0417, 
epoch 18, test loss is 0.1613,  test sen is 0.0043, test spe is 0.4687, test F1 is nan, test acc is 0.0166, 
epoch 22, test loss is 0.1563,  test sen is 0.0409, test spe is 0.7024, test F1 is 0.0738, test acc is 0.0278, 
epoch 25, test loss is 0.1568,  test sen is 0.1045, test spe is 0.6708, test F1 is 0.1515, test acc is 0.0279, 
epoch 26, test loss is 0.1533,  test sen is 0.1164, test spe is 0.6363, test F1 is 0.1657, test acc is 0.0247, 
epoch 27, test loss is 0.1486,  test sen is 0.1688, test spe is 0.6560, test F1 is 0.2029, test acc is 0.0339, 
epoch 28, test loss is 0.1381,  test sen is 0.2013, test spe is 0.6461, test F1 is 0.2404, test acc is 0.0110, 
epoch 30, test loss is 0.1368,  test sen is 0.1936, test spe is 0.6760, test F1 is 0.2338, test acc is 0.0211, 
epoch 31, test loss is 0.1370,  test sen is 0.2065, test spe is 0.6653, test F1 is 0.2455, test acc is 0.0197, 
epoch 35, test loss is 0.1365,  test sen is 0.1940, test spe is 0.6011, test F1 is 0.2237, test acc is 0.0318, 
epoch 36, test loss is 0.1234,  test sen is 0.2414, test spe is 0.7333, test F1 is 0.2927, test acc is 0.0152, 
epoch 41, test loss is 0.1231,  test sen is 0.2191, test spe is 0.6779, test F1 is 0.2672, test acc is 0.0315, 
epoch 42, test loss is 0.1203,  test sen is 0.2179, test spe is 0.6524, test F1 is 0.2610, test acc is 0.0335, 
epoch 43, test loss is 0.1147,  test sen is 0.2357, test spe is 0.7138, test F1 is 0.2846, test acc is 0.0314, 
epoch 45, test loss is 0.1116,  test sen is 0.2426, test spe is 0.6872, test F1 is 0.2987, test acc is 0.0327, 
epoch 47, test loss is 0.1146,  test sen is 0.2473, test spe is 0.6639, test F1 is 0.3041, test acc is 0.0362, 
epoch 0, test loss is 0.4168,  test sen is 0.0001, test spe is 0.1554, test F1 is nan, test acc is 0.0095, 
epoch 2, test loss is 0.3257,  test sen is 0.0002, test spe is 0.2918, test F1 is nan, test acc is 0.0155, 
epoch 3, test loss is 0.2756,  test sen is 0.0000, test spe is 0.2000, test F1 is nan, test acc is 0.0165, 
epoch 4, test loss is 0.2661,  test sen is 0.0000, test spe is 0.0381, test F1 is nan, test acc is 0.0200, 
epoch 5, test loss is 0.2163,  test sen is 0.0000, test spe is 0.0000, test F1 is nan, test acc is 0.0686, 
epoch 6, test loss is 0.2112,  test sen is 0.0000, test spe is 0.0000, test F1 is nan, test acc is 0.0924, 
epoch 7, test loss is 0.2072,  test sen is 0.0000, test spe is 0.0000, test F1 is nan, test acc is 0.1170, 
epoch 8, test loss is 0.2012,  test sen is 0.0000, test spe is 0.0000, test F1 is nan, test acc is 0.1272, 
epoch 11, test loss is 0.1794,  test sen is 0.0000, test spe is 0.0000, test F1 is nan, test acc is 0.1426, 
epoch 12, test loss is 0.1833,  test sen is 0.0000, test spe is 0.0000, test F1 is nan, test acc is 0.1559, 
epoch 14, test loss is 0.1840,  test sen is 0.0000, test spe is 0.2000, test F1 is nan, test acc is 0.1645, 
epoch 161, test loss is 0.2431,  test sen is 0.3395, test spe is 0.7723, test F1 is 0.3778, test acc is 0.1786, 
best loss is 0.1038, best F1 is 0.5355, best acc is 0.1786, 
learning rate 0.0002, Max Epoch is 200
epoch 0, test loss is 0.3934,  test sen is 0.0003, test spe is 0.0880, test F1 is nan, test acc is 0.2729, 
best loss is 0.2987, best F1 is 0.0000, best acc is 0.2729, 
learning rate 0.0002, Max Epoch is 2
