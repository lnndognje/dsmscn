import argparse
import cv2 as cv
import os
import pickle
# 
import time
import numpy as np
from random import seed
# 
from keras.optimizers import Adam
from keras.callbacks import *
#
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
# option2
from seg_model.MyModel.SiameseInception_Keras import SiameseInception
#
from acc_util import Recall, Precision, F1_score
from seg_model.U_net.FC_Siam_Diff import get_FCSD_model
from seg_model.U_net.FC_Siam_Conc import get_FCSC_model
from seg_model.U_net.FC_EF import get_FCEF_model
from acc_ass import accuracy_assessment
from net_util import weight_binary_cross_entropy
from scipy import misc

parser = argparse.ArgumentParser()
parser.add_argument('--max_epoch', type=int, default=100, help='epoch to run[default: 100]')
parser.add_argument('--batch_size', type=int, default=8, help='batch size during training[default: 512]')
parser.add_argument('--learning_rate', type=float, default=2e-4, help='initial learning rate[default: 3e-4]')
parser.add_argument('--result_save_path', default='./result', help='model param path')
parser.add_argument('--model_save_path', default='./model_param', help='model param path')
parser.add_argument('--data_path', default='data', help='data path')
parser.add_argument('--data_set_name', default='ACD/Szada', help='dataset name')
parser.add_argument('--gpu_num', type=int, default=1, help='number of GPU to train')

# basic params
FLAGS = parser.parse_args()

BATCH_SZ = FLAGS.batch_size
LEARNING_RATE = FLAGS.learning_rate
MAX_EPOCH = FLAGS.max_epoch
RESULT_SAVE_PATH = FLAGS.result_save_path
MODEL_SAVE_PATH = FLAGS.model_save_path
DATA_PATH = FLAGS.data_path
DATA_SET_NAME = FLAGS.data_set_name
GPU_NUM = FLAGS.gpu_num
BATCH_PER_GPU = BATCH_SZ // GPU_NUM
# 
timestr = time.strftime("%Y%m%d-%H%M%S")
# 
def train():
    # result path definition
    result_path = os.path.join(RESULT_SAVE_PATH, DATA_SET_NAME)
    # model path definition
    model_save_path = os.path.join(MODEL_SAVE_PATH, DATA_SET_NAME)
    if not os.path.exists(result_path):
        os.makedirs(result_path)
    if not os.path.exists(model_save_path):
        os.makedirs(model_save_path)
    train_X, train_Y, train_label, test_X, test_Y, test_label = get_data()
    # length of training and test sets
    nr_train = len(train_Y)
    print("lenght of trainx: ", nr_train)
    print("shape of one training sample: ", train_Y[0].shape)
    nr_test = len(test_X)
    # reshaping training input
    train_X = np.reshape(np.vstack(tuple(train_X)), [nr_train, train_X[0].shape[0], train_X[0].shape[1], train_X[0].shape[2]])
    train_Y = np.reshape(np.asarray(train_Y), [nr_train, train_Y[0].shape[0], train_Y[0].shape[1], train_Y[0].shape[2]])
    train_label = np.reshape(np.asarray(train_label), [nr_train, train_X[0].shape[0], train_X[0].shape[1]])
    # reshaping validation input
    test_X = np.reshape(np.vstack(tuple(test_X)), [nr_test, test_X[0].shape[0], test_X[0].shape[1], test_X[0].shape[2]])
    test_Y = np.reshape(np.asarray(test_Y), [nr_test, test_Y[0].shape[0], test_Y[0].shape[1], test_Y[0].shape[2]])
    test_label = np.reshape(np.asarray(test_label), [nr_test, test_X[0].shape[0], test_X[0].shape[1]])
    # model definition
    #option2
    si_model = SiameseInception()
    deep_model = si_model.get_model(input_size=[None, None, 3])
    #
    # deep_model = get_FCSC_model(input_size=[None, None, 3]) #get_FCSC_model
    # optimizer settings
    opt = Adam(lr=LEARNING_RATE)
    # compiling model
    deep_model.compile(optimizer=opt, loss=weight_binary_cross_entropy, metrics=['accuracy', Recall, Precision, F1_score])
    # summarize architecture of the network
    deep_model.summary()
    # model checkpoint definition - val_loss or val_acc
    checkpoint = ModelCheckpoint(os.path.join(model_save_path, 'Model_Siamese_' + timestr + '.h5'), monitor = 'val_Precision', verbose = 1, save_best_only = True, mode = 'max')
    # logger #Model_Siamese_20210826-043021.h5
    csv_logger = CSVLogger(os.path.join(model_save_path, 'Model_Siamese_' + timestr + '_log.out'), append = True, separator = ';')
    # learning rate adjustments
    reduce_lr = ReduceLROnPlateau(factor = 0.1, patience = 7, min_lr = 0.000001, verbose = 1)
    # build callback from parameters
    callbacks_list = [checkpoint, csv_logger, reduce_lr]
    # model launching
    ch_detector = deep_model.fit([train_X, train_Y], train_label, validation_data = ([test_X, test_Y], test_label), batch_size = BATCH_SZ, epochs = MAX_EPOCH, callbacks = callbacks_list)
    #print headers of all history available #['val_loss', 'val_acc', 'val_Recall', 'val_Precision', 'val_F1_score', 'loss', 'acc', 'Recall', 'Precision', 'F1_score', 'lr']
    print(ch_detector.history.keys())
    # training graph drawing focused on loss evolution
    loss = ch_detector.history['loss']
    # 
    val_loss = ch_detector.history['val_loss']
    # 
    epochx = range(1, len(loss) + 1)
    # 
    plt.plot(epochx, loss, label = 'training loss')
    # 
    plt.plot(epochx, val_loss, label = 'validation loss')
    # 
    plt.title('Training and validation loss')
    # 
    plt.legend()
    # save figure
    plt.savefig(os.path.join(result_path, str('Display_train_validation') + 'Model_Siamese_' + timestr + '_loss.png'))#Unet - Siamese
    # 
    plt.figure()
    # 
    # training graph drawing focused on accuracy evolution
    acc = ch_detector.history['Precision']
    # 
    val_acc = ch_detector.history['val_Precision']
    # 
    plt.plot(epochx, acc, label = 'training Precision')
    # 
    plt.plot(epochx, val_acc, label = 'validation Precision')
    # 
    plt.title('Training and validation Precision history')
    # 
    plt.legend()
    # save figure
    plt.savefig(os.path.join(result_path, str('Display_train_validation') + 'Model_Siamese_' + timestr + '_precision.png'))
# 
def get_data():
    path = os.path.join(DATA_PATH, DATA_SET_NAME)
    train_X, train_Y, train_label = load_train_data(path=path)
    test_X, test_Y, test_label = load_test_data(path=path)

    # test_X = np.array(test_X) / 255.
    # test_Y = np.array(test_Y) / 255.
    # test_label = np.array(test_label) / 255.
    # test_label = np.reshape(test_label, (test_label.shape[0], test_label.shape[1], test_label.shape[2]))
    print(type(test_X))
    print(type(train_X))
    for j in range(len(test_X)):
        test_X[j] = test_X[j] / 255.
        test_Y[j] = test_Y[j] / 255.
        test_label[j] = test_label[j]#/ 255.
    for i in range(len(train_X)):
        train_X[i] = train_X[i] / 255.
        train_Y[i] = train_Y[i] / 255.
        train_label[i] = train_label[i]# / 255. #remove the division for oscd data labels

    return train_X, train_Y, train_label, test_X, test_Y, test_label

def data_gen(im_folder, batch_size):
    # 
    c = 0
    n1 = [file for file in os.listdir(im_folder) if 'im1' in file]
    random.shuffle(n1)
    # 
    np.list = []
    # 
    yield im1, im2, label

def load_train_data(path):
    with open(os.path.join(path, 'train_sample_1.pickle'), 'rb') as file:
        train_X = pickle.load(file)
    with open(os.path.join(path, 'train_sample_2.pickle'), 'rb') as file:
        train_Y = pickle.load(file)
    with open(os.path.join(path, 'train_label.pickle'), 'rb') as file:
        train_label = pickle.load(file)

    return train_X, train_Y, train_label


def load_test_data(path):
    with open(os.path.join(path, 'test_sample_1.pickle'), 'rb') as file:
        test_X = pickle.load(file)
    with open(os.path.join(path, 'test_sample_2.pickle'), 'rb') as file:
        test_Y = pickle.load(file)
    with open(os.path.join(path, 'test_label.pickle'), 'rb') as file:
        test_label = pickle.load(file)

    return test_X, test_Y, test_label


if __name__ == '__main__':
    # train_model()
    train()
