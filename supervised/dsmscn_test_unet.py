# Laurent NL. inference script on DSMSCN dataset using supervised trained model
import rasterio
from rasterio.windows import Window
# add for file writing with metadata
import tensorflow as tf
import argparse
import os
import cv2 as cv
import pickle
import numpy as np
from keras.models import load_model
from keras.optimizers import Adam

from acc_util import Recall, Precision, F1_score
from seg_model.U_net.FC_Siam_Diff import get_FCSD_model
from seg_model.U_net.FC_Siam_Conc import get_FCSC_model
from seg_model.U_net.FC_EF import get_FCEF_model
from acc_ass import accuracy_assessment
from net_util import weight_binary_cross_entropy
from scipy import misc

parser = argparse.ArgumentParser()
parser.add_argument('--max_epoch', type=int, default=200, help='epoch to run[default: 100]')
parser.add_argument('--batch_size', type=int, default=1, help='batch size during training[default: 512]')
parser.add_argument('--learning_rate', type=float, default=2e-4, help='initial learning rate[default: 3e-4]')
parser.add_argument('--result_save_path', default='./result', help='model param path')
parser.add_argument('--model_save_path', default='./model_param', help='model param path')
parser.add_argument('--data_path', default='data', help='data path')
parser.add_argument('--data_set_name', default='ACD/Szada', help='dataset name')
parser.add_argument('--gpu_num', type=int, default=1, help='number of GPU to train')

# basic params
FLAGS = parser.parse_args()

BATCH_SZ = FLAGS.batch_size
LEARNING_RATE = FLAGS.learning_rate
MAX_EPOCH = FLAGS.max_epoch
RESULT_SAVE_PATH = FLAGS.result_save_path
MODEL_SAVE_PATH = FLAGS.model_save_path
DATA_PATH = FLAGS.data_path
DATA_SET_NAME = FLAGS.data_set_name
GPU_NUM = FLAGS.gpu_num
BATCH_PER_GPU = BATCH_SZ // GPU_NUM
#
print('Parametrisation done...')

# read dataset

def read_data():
    result_path = os.path.join(RESULT_SAVE_PATH, DATA_SET_NAME)
    path = './data/ACD/Szada/testx'
    # check if pickle files already exists; if it's the case, delete them and recreate them.
    if os.path.exists('data/ACD/Szada/testx_sample_1.pickle'):
        os.remove('data/ACD/Szada/testx_sample_1.pickle')
        print('removed existing testx sample 1')
    if os.path.exists('data/ACD/Szada/testx_sample_2.pickle'):
        os.remove('data/ACD/Szada/testx_sample_2.pickle')
        print('removed existing testx sample 2')
    if os.path.exists('data/ACD/Szada/testx_label.pickle'):
        os.remove('data/ACD/Szada/testx_label.pickle')
        print('removed existing testx label')
    train_img_1 = []
    train_img_2 = []
    train_label = []
    file_names = sorted(os.listdir(path))
    print(file_names)
    for file_name in file_names:
        if file_name[-4:].lower() == '.tif':#'.BMP'
            print(file_name)
            img = cv.imread(os.path.join(path, file_name))
            with rasterio.open(os.path.join(path, file_name)) as src:
                profile = src.profile.copy()
                window = Window(0, 0, 1024, 1024)
                transform = src.window_transform(window)
                profile.update({'height' : 1024, 'width' : 1024, 'transform' : transform})#blockxsize=256, blockysize=256, tiled='yes'#height=1024, width=1024
                with rasterio.open(os.path.join(result_path, file_name.replace('.tif','')+'_resized.tif'), 'w', **profile) as dst:
                    dst.write(src.read(window=window)) #(img[:1024, :1024, 2])
            if img.shape[0] > img.shape[1]:
                img = img[:1024,:1024,:] # [0:784, :, :]
            elif img.shape[0] < img.shape[1]:
                img = img[:1024,:1024,:] # [:, 0:784, :]
            if 'gt.tif' in file_name.lower():
                img = cv.cvtColor(img, cv.COLOR_RGB2GRAY)
                train_label.append(img)
            elif 'im1.tif' in file_name.lower():
                train_img_1.append(img)
            elif 'im2.tif' in file_name.lower():
                train_img_2.append(img)
    # if not os.path.exists('data/ACD/Szada/test_sample_1.pickle'):
    #     os.mkdir('data/ACD/Szada/')
    with open('data/ACD/Szada/testx_sample_1.pickle', 'wb') as file:
        pickle.dump(train_img_1, file)
    # if not os.path.exists('data/ACD/Szada/test_sample_2.pickle'):
    #     os.mkdir('data/ACD/Szada/')
    with open('data/ACD/Szada/testx_sample_2.pickle', 'wb') as file:
        pickle.dump(train_img_2, file)
    # if not os.path.exists('data/ACD/Szada/test_label.pickle'):
    #     os.mkdir('data/ACD/Szada/')
    with open('data/ACD/Szada/testx_label.pickle', 'wb') as file:
        pickle.dump(train_label, file)
    #
    print('Reading data ok')

# 
# load model

def test_model():
    result_path = os.path.join(RESULT_SAVE_PATH, DATA_SET_NAME)
    model_save_path = os.path.join(MODEL_SAVE_PATH, DATA_SET_NAME)
    if not os.path.exists(result_path):
        os.makedirs(result_path)
    if not os.path.exists(model_save_path):
        os.makedirs(model_save_path)

    test_X, test_Y, test_label = get_data()
    print("Shape of the test label: ", test_label.shape)
    
    # load model
    deep_model = load_model(os.path.join(model_save_path, 'Model_Siamese_20210826-041609.h5'), custom_objects={'tf':tf}, compile=False)
    # loaded_model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
    # summarize model. Model_Unet_20210825-095457.h5 #str(100) + '_model.h5'
    # deep_model.summary() #uncomment
    # load dataset
    # dataset = loadtxt("pima-indians-diabetes.csv", delimiter=",")
    # split into input (X) and output (Y) variables
    # X = dataset[:,0:8]
    # Y = dataset[:,8]
    print(test_X.shape)
    print(test_Y.shape)
    # evaluate the model
    # score = model.evaluate(X, Y, verbose=0)
    if True:
        # loss, acc, sen, spe, F1 = deep_model.evaluate(x=[test_X, test_Y], y=test_label, batch_size=1)
        # loss, acc, sen, spe, F1 = deep_model.evaluate(x=test, y=test_label, batch_size=1) # FC-EF

        binary_change_map = deep_model.predict([test_X, test_Y])
        #  binary_change_map = deep_model.predict(test)

        binary_change_map = np.reshape(binary_change_map, (binary_change_map.shape[1], binary_change_map.shape[2]))
        idx_1 = binary_change_map > 0.5
        idx_2 = binary_change_map <= 0.5
        binary_change_map[idx_1] = 255
        binary_change_map[idx_2] = 0

        #conf_mat, overall_acc, kappa = accuracy_assessment(
        #gt_changed=np.reshape(255 * test_label, (test_label.shape[1], test_label.shape[2])),
        #gt_unchanged=np.reshape(255. - 255 * test_label, (test_label.shape[1], test_label.shape[2])),
        #changed_map=binary_change_map)

        # info = 'Test acc is %.4f, test kappa is %.4f, ' % (overall_acc, kappa)
        # info = 'no info'
        # print(info)
        # print('confusion matrix is ', conf_mat)
        # test process complete
        #
        test_path = result_path#'./data/ACD/Szada/testx'
        test_file = sorted([file for file in os.listdir(result_path) if 'resized' in file])[-1]
        with rasterio.open(os.path.join(test_path, test_file)) as src:
            profile = src.profile.copy()
            profile.update(count=1)
            with rasterio.open(os.path.join(result_path, 'cm_Model_Siamese_20210826-041609_201802to201901b.tif'), 'w', **profile) as dst:
                dst.write(binary_change_map*255, indexes=1)
        # with open(os.path.join(result_path, 'log_test_0.txt'), 'a+') as f:
        # f.write(info + '\n')
        # cv.imwrite(os.path.join(result_path, str(100) + '_model_cm_201802to201901.tif'), binary_change_map)
        # f.close()

def get_data():
    path = os.path.join(DATA_PATH, DATA_SET_NAME)
    test_X, test_Y, test_label = load_test_data(path=path)

    test_X = np.array(test_X) / 255.
    test_Y = np.array(test_Y) / 255.
    test_label = np.array(test_label) / 255.
    # test_label = np.reshape(test_label, (test_label.shape[0], test_label.shape[1]))
    

    return test_X, test_Y, test_label



def load_test_data(path):
    with open(os.path.join(path, 'testx_sample_1.pickle'), 'rb') as file:
        test_X = pickle.load(file)
    with open(os.path.join(path, 'testx_sample_2.pickle'), 'rb') as file:
        test_Y = pickle.load(file)
    with open(os.path.join(path, 'testx_label.pickle'), 'rb') as file:
        test_label = pickle.load(file)

    return test_X, test_Y, test_label


if __name__ == '__main__':
    read_data()
    #
    # test_model()
